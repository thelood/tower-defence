/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TowerDefenceGame;

/**
 *
 * @author thelood
 */
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import javax.imageio.ImageIO;

public class DrawImageOnCanvas implements Runnable {

    private Display Display;
    private Thread t;
    private boolean running;
    private boolean once;
    private final String title;
    private BufferStrategy bs;
    private Graphics g;
    private ArrayList<BufferedImage> images = new ArrayList<>();
Map map;
    int rows;
    int columns;
    int pathstart;
        Random generator = new Random();
        
    // Images for each animation
    // private BufferedImage[] walkingLeft;
    //  private BufferedImage[] walkingRight = {Sprite.getSprite(0, 2), Sprite.getSprite(2, 2)};
    private BufferedImage[] walkingForward = {Sprite.getSprite(76, 0), Sprite.getSprite(76, 38), Sprite.getSprite(76, 76)};

// These are animation states
    // private Animation walkLeft;
    // private Animation walkRight = new Animation(walkingRight, 10);
    private Animation walkForward = new Animation(walkingForward, 60);

// This is the actual animation
    private Animation animation = walkForward;

    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    public DrawImageOnCanvas(String title, int rows, int columns) {
        this.title = title;
        this.rows = rows;
        this.columns = columns;
        this.pathstart = (generator.nextInt((columns - 3)) + 1);
        this.map = new Map(columns, rows, pathstart);
    }

    @Override
    public void run() {
        init();
        while (running) {
            animation.update();
            render();
        }
        //stop();
    }

    private void render() {
        int tileheight = images.get(0).getHeight();
        bs = Display.getCanvas().getBufferStrategy();
        if (bs == null) {
            Display.getCanvas().createBufferStrategy(3);
            Display.getCanvas().addMouseMotionListener(new MouseMotionListener() {
                @Override
                public void mouseDragged(MouseEvent e) {
                }

                @Override
                public void mouseMoved(MouseEvent e) {
                    //  System.out.println(e.getY());
                }
            });
            return;
        }
        g = Display.getCanvas().getGraphics();
        int h = 9;
        int w = 0;
        for (int l = 0; l < images.size(); l++) {
            g.drawImage(images.get(l), tileheight * w, tileheight * h, null);
            if (w < columns - 1) {
                w++;
            } else {
                w = 0;
                if (h > 0) {
                    h--;
                }
            }
        }
        g.drawImage(animation.getSprite(), (pathstart*images.get(0).getWidth())+images.get(0).getWidth(),(rows*images.get(0).getHeight())-images.get(0).getHeight(), null);
        //  running = false;
    }

    private void init() {
        images = map.getBgImages();
        Display = new Display(title, images.get(0).getHeight() * rows, images.get(0).getWidth() * 6);
    }

    public synchronized void start() {
        if (running) {
            return;
        }
        running = true;
        t = new Thread(this);
        t.start();
    }

    public synchronized void stop() {
        if (!running) {
            return;
        }
        running = false;
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
