/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TowerDefenceGame;

import java.awt.image.BufferedImage;

/**
 *
 * @author thelood
 */
public class Enemy {
    // Images for each animation
private BufferedImage[] walkingLeft = {Sprite.getSprite(0, 1), Sprite.getSprite(2, 1)}; 
private BufferedImage[] walkingRight = {Sprite.getSprite(0, 2), Sprite.getSprite(2, 2)};
private BufferedImage[] walkingForward = {Sprite.getSprite(1, 0)};

// These are animation states
private Animation walkLeft = new Animation(walkingLeft, 10);
private Animation walkRight = new Animation(walkingRight, 10);
private Animation walkForward = new Animation(walkingForward, 10);

// This is the actual animation
private Animation animation = walkForward;


}
