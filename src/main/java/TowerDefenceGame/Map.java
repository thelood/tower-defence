/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TowerDefenceGame;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author thelood
 */
public class Map {

    private ArrayList<BufferedImage> BgImages;
    private final ArrayList<BufferedImage> op;
    private final ArrayList<BufferedImage> og;
    private final ArrayList<BufferedImage> sp;
    private final ArrayList<BufferedImage> turns;

    private final int columns;
    private final int rows;
    private int firstrowpath;
    private int coordturn;
    
    Random generator;

    public Map(int columns, int rows, int pathstart) {
        this.generator = new Random();
        this.sp = new ArrayList<>();
        this.og = new ArrayList<>();
        this.BgImages = new ArrayList<>();
        this.op = new ArrayList<>();
        this.turns = new ArrayList<>();
        this.columns = columns;
        this.rows = rows;
        this.coordturn = 0;
        this.firstrowpath = pathstart;
        loadArrays();
        BgImages = generateMap();
    }

    public ArrayList<BufferedImage> getBgImages() {
        return BgImages;
    }

    public void setBgImages(ArrayList<BufferedImage> BgImages) {
        this.BgImages = BgImages;
    }

    private ArrayList<BufferedImage> generateMap() {
        //0 = straight
        //1 = left hand turn
        //2 = right hand turn
        //turn goes to row 4
        //turn2 goes from row 4 to row 7 
        int turn = generator.nextInt(3);
        int turn2 = generator.nextInt(3);
        //path generation row detection needs to become dynamic before fucking with the map
//          while (BgImages.size() < 12) {
//            BgImages.add(op.get(generator.nextInt(5)));
//        }
        while (BgImages.size() < 60) {
            BgImages.add(og.get(generator.nextInt(5)));
        }
        BgImages.set(firstrowpath, sp.get(0));
        BgImages.set(1 + firstrowpath, sp.get(1));
        switch (turn) {
            case 0:
                //straight
                BgImages.set(6 + firstrowpath, sp.get(0));
                BgImages.set(7 + firstrowpath, sp.get(1));
                BgImages.set(12 + firstrowpath, sp.get(0));
                BgImages.set(firstrowpath + 13, sp.get(1));
                BgImages.set(firstrowpath + 18, sp.get(0));
                BgImages.set(firstrowpath + 19, sp.get(1));
                coordturn = firstrowpath + 18;
                break;
            case 1:
                //turn left
                BgImages.set(firstrowpath + 6, turns.get(0));
                BgImages.set(firstrowpath + 7, sp.get(1));
                BgImages.set(firstrowpath + 13, turns.get(2));
                //left boundary
                if (firstrowpath == 1) {
                    BgImages.set(firstrowpath + 5, turns.get(6));
                    BgImages.set(firstrowpath + 11, sp.get(0));
                    BgImages.set(firstrowpath + 12, turns.get(8));
                    BgImages.set(firstrowpath + 17, sp.get(0));
                    BgImages.set(firstrowpath + 18, sp.get(1));
                    coordturn = firstrowpath + 17;
                }
                if (firstrowpath != 1) {
                    BgImages.set(firstrowpath + 4, turns.get(6));
                    BgImages.set(firstrowpath + 5, turns.get(4));
                    BgImages.set(firstrowpath + 10, sp.get(0));
                    BgImages.set(firstrowpath + 11, turns.get(8));
                    BgImages.set(firstrowpath + 12, turns.get(5));
                    BgImages.set(firstrowpath + 16, sp.get(0));
                    BgImages.set(firstrowpath + 17, sp.get(1));
                    coordturn = firstrowpath + 16;
                }
                break;
            case 2:
                //turn right
                BgImages.set(firstrowpath + 6, sp.get(0));
                BgImages.set(firstrowpath + 7, turns.get(1));
                BgImages.set(firstrowpath + 12, turns.get(3));
                //right boundary
                if (firstrowpath == 3) {
                    BgImages.set(firstrowpath + 8, turns.get(7));
                    BgImages.set(firstrowpath + 13, turns.get(9));
                    BgImages.set(firstrowpath + 14, sp.get(1));
                    BgImages.set(firstrowpath + 19, sp.get(0));
                    BgImages.set(firstrowpath + 20, sp.get(1));
                    coordturn = firstrowpath + 19;
                }
                if (firstrowpath != 3) {
                    BgImages.set(firstrowpath + 8, turns.get(4));
                    BgImages.set(firstrowpath + 9, turns.get(7));
                    BgImages.set(firstrowpath + 13, turns.get(5));
                    BgImages.set(firstrowpath + 14, turns.get(9));
                    BgImages.set(firstrowpath + 15, sp.get(1));
                    BgImages.set(firstrowpath + 20, sp.get(0));
                    BgImages.set(firstrowpath + 21, sp.get(1));
                    coordturn = firstrowpath + 20;
                    break;
                }
        }
        addPath(turn2, coordturn);
        System.out.println("First: " + firstrowpath + " Turn: " + turn + " Turn2: " + turn2);
        return BgImages;
    }

    public void addPath(int turn, int place) {
        //no row detection yet...
        //boundry detection from turn coordinate
        if ((place - 17 == 1) && turn == 1) {
            while (turn == 1) {
                turn = generator.nextInt(3);
            }
        }
        if ((place - 19 == 3) && turn == 2) {
            while (turn == 2) {
                turn = generator.nextInt(3);
            }
        }
        switch (turn) {
            //straight
            case 0:
                BgImages.set(place + 6, sp.get(0));
                BgImages.set(place + 7, sp.get(1));
                BgImages.set(place + 12, sp.get(0));
                BgImages.set(place + 13, sp.get(1));
                BgImages.set(place + 18, sp.get(0));
                BgImages.set(place + 19, sp.get(1));
                break;
            //left
            case 1:
                //turn left
                BgImages.set(place + 6, turns.get(0));
                BgImages.set(place + 7, sp.get(1));
                BgImages.set(place + 13, turns.get(2));
                //left boundary
                if ((place - 17) == 1) {
                    BgImages.set(place + 5, turns.get(6));
                    BgImages.set(place + 11, sp.get(0));
                    BgImages.set(place + 12, turns.get(8));
                    BgImages.set(place + 17, sp.get(0));
                    BgImages.set(place + 18, sp.get(1));
                }
                if ((place - 18) == 1) {
                    BgImages.set(place + 5, turns.get(6));
                    BgImages.set(place + 11, sp.get(0));
                    BgImages.set(place + 12, turns.get(8));
                    BgImages.set(place + 17, sp.get(0));
                    BgImages.set(place + 18, sp.get(1));
                } else if ((place - 17) != 1) {
                    BgImages.set(place + 4, turns.get(6));
                    BgImages.set(place + 5, turns.get(4));
                    BgImages.set(place + 10, sp.get(0));
                    BgImages.set(place + 11, turns.get(8));
                    BgImages.set(place + 12, turns.get(5));
                    BgImages.set(place + 16, sp.get(0));
                    BgImages.set(place + 17, sp.get(1));
                }
                break;
            //right
            case 2:
                //turn right
                BgImages.set(place + 6, sp.get(0));
                BgImages.set(place + 7, turns.get(1));
                BgImages.set(place + 12, turns.get(3));
                //right boundary
                if ((place - 18) == 3) {
                    BgImages.set(place + 8, turns.get(7));
                    BgImages.set(place + 13, turns.get(9));
                    BgImages.set(place + 14, sp.get(1));
                    BgImages.set(place + 19, sp.get(0));
                    BgImages.set(place + 20, sp.get(1));
                }
                if ((place - 19) == 3) {
                    BgImages.set(place + 5, turns.get(6));
                    BgImages.set(place + 11, sp.get(0));
                    BgImages.set(place + 12, turns.get(7));
                    BgImages.set(place + 17, sp.get(0));
                    BgImages.set(place + 18, sp.get(1));
                } else if ((place - 18) != 3) {
                    BgImages.set(place + 8, turns.get(4));
                    BgImages.set(place + 9, turns.get(7));
                    BgImages.set(place + 13, turns.get(5));
                    BgImages.set(place + 14, turns.get(9));
                    BgImages.set(place + 15, sp.get(1));
                    BgImages.set(place + 20, sp.get(0));
                    BgImages.set(place + 21, sp.get(1));
                }
                break;
        }
    }

    private void loadArrays() {
        File folder = new File("./src/main/resources/textures");
        File[] listOfFiles = folder.listFiles();
        try {
            for (File listOfFile : listOfFiles) {
                if (listOfFile.toString().contains("og")) {
                    og.add(ImageIO.read(this.getClass().getResource("/textures/" + listOfFile.getName())));
                }
                if (listOfFile.toString().contains("op")) {
                    op.add(ImageIO.read(this.getClass().getResource("/textures/" + listOfFile.getName())));
                }
                if (listOfFile.toString().contains("sp")) {
                    sp.add(ImageIO.read(this.getClass().getResource("/textures/" + listOfFile.getName())));
                }
                if (listOfFile.toString().contains("turns")) {
                    turns.add(ImageIO.read(this.getClass().getResource("/textures/" + listOfFile.getName())));
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
