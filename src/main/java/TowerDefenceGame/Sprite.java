package TowerDefenceGame;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

public class Sprite {

    private static BufferedImage spriteSheet;

    public static BufferedImage loadSprite(String file) {
                BufferedImage sprite = null;
        try {
            sprite = ImageIO.read(
                    Sprite.class.getResource("/Creatures/" + file + ".png"));
        } catch (IOException ex) {
            Logger.getLogger(Sprite.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sprite;
    }

    public static BufferedImage getSprite(int xGrid, int yGrid) {

        if (spriteSheet == null) {
            spriteSheet = loadSprite("DireRat");
        }

        return spriteSheet.getSubimage(xGrid, yGrid, 38, 38);
    }

}